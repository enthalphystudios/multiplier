//
//  ViewController.swift
//  multiplier
//
//  Created by Marco Rodrigues on 05/02/2016.
//  Copyright © 2016 Marco Rodrigues. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var multiplier: Int = 0
    var currentMultiplier: Int = 0
    let max: Int = 100
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var imgMultiplier: UIImageView!
    @IBOutlet weak var txtMultiples: UITextField!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblAdd: UILabel!
    
    @IBAction func onPlay(sender: UIButton!) {
        if (txtMultiples.text != nil && txtMultiples.text != "") {
            multiplier = Int(txtMultiples.text!)!
            showTitleView(false)
            currentMultiplier = 0
        }
    }
    
    @IBAction func onAdd(sender: UIButton!) {
        let sum: Int = calc(currentMultiplier, multiplier)
        lblAdd.text = "\(currentMultiplier) + \(multiplier) = \(sum)"
        currentMultiplier = sum
        if (currentMultiplier >= max) {
            reset()
        }
    }
    
    func reset() {
        showTitleView(true)
        currentMultiplier = 0
        multiplier = 0
        lblAdd.text = "Press Add to Add!"
    }
    
    func calc(a: Int, _ b: Int) -> Int {
        return a + b
    }
    
    func showTitleView(show: Bool) {
        btnPlay.hidden = !show
        txtMultiples.hidden = !show
        imgMultiplier.hidden = !show
        lblAdd.hidden = show
        btnAdd.hidden = show
    }

}

